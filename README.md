# Mreplace
  Multiple file text append/replace script.


## Setup

1.  Save mreplace file in:
        ```
        /usr/local/bin
        ```
        
2.  Allow execution with:
        ```
        chmod u+x mreplace
        ```
        
3.  To run at the command line, specify -a to append after, -r to replace, use:
        ```
        mreplace -a 'string to find' 'new string' < ./ListOfFilesToChange
        ```

## Notes

* **Usage**: -mode 'string to find' 'new string' < ./listofinputfiles.txt


* **Available modes**:

      -a, -append   Append immediately after string

      -r, -replace  Replace original string with new string
      

* **Example usage**:

    ```
    mreplace -append 'string to find' 'new string' < ./ListOfFilesToChange.txt
    ```
    
* **Suggested input file format**: in the above example, ./ListOfFilestoChange.txt should have the format below. Other formats may work but have not been tested.

   
            
            ./replacetest.txt 
    
            ./testdoc.txt
    
            ./testdoc2.txt
    
            ./testdoc3.txt
    
            ./testdoc4.txt
    
            ./testdoc5.txt
    
           


## Copyright

Copyright 2016 Western Washington University Web Technologies Group Licensed
under the Educational Community License, Version 2.0 (the "License") you may not
use this file except in compliance with the License. You may obtain a copy of
the License at http://www.osedu.org/licenses/ECL-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
